`timescale 1 ns / 1 ps
module MuxP4(input a,b,s, output w);
  wire j, k ,l;
  nandP1 N1(s,s,j);
  nandP1 N2(j,a,k);
  nandP1 N3(s,b,l);
  nandP1 N4(k,l,w);
endmodule

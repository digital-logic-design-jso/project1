`timescale 1 ns / 1 ps
module MuxP7 (input a,b,s ,output w);
  pmos #(5,6,7) T1(w,a,s);
  nmos #(3,4,5) T2(w,b,s);
endmodule
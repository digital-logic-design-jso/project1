`timescale 1 ns / 1 ps
module MuxP6 (input a,b,s, output w);
  wire j,k,l,m,n,o;
  supply1 vdd;
  supply0 Gnd;
  
  //inverters
  pmos #(5,6,7) T1(n,vdd,a);
  nmos #(3,4,5) T2(n,Gnd,a);
  
  pmos #(5,6,7) T3(l,vdd,b);
  nmos #(3,4,5) T4(l,Gnd,b);
  
  pmos #(5,6,7) T5(m,vdd,s);
  nmos #(3,4,5) T6(m,Gnd,s);
  //mux
  pmos #(5,6,7) T7(o,vdd,n);
  pmos #(5,6,7) T8(w,o,s);
  pmos #(5,6,7) T9(k,vdd,l);
  pmos #(5,6,7) T10(w,k,m);
  
  nmos #(3,4,5) T11(j,Gnd,l);
  nmos #(3,4,5) T12(j,Gnd,m);
  nmos #(3,4,5) T13(w,j,n);
  nmos #(3,4,5) T14(w,j,s);
  
endmodule

`timescale 1 ns / 1 ps
module nandP1TB();
  reg aa,bb;
  wire ww;
  nandP1 G2(aa,bb,ww);
  initial begin
    aa=1;
    bb=0;
    #37;
    bb=1;
    #57;
    bb=0;
    #43;
    $stop;
  end
endmodule
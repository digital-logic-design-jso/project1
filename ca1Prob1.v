`timescale 1 ns / 1 ps
module nandP1 (input a, b, output w);
  supply1 vdd;
  supply0 Gnd;
  wire j;
  pmos #(5,6,7) T1(w,vdd,a);
  pmos #(5,6,7) T2(w,vdd,b);
  nmos #(3,4,5) T3(w,j,a);
  nmos #(3,4,5) T4(j,Gnd,b);
endmodule

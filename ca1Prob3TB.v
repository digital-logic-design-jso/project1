`timescale 1 ns / 1 ps
module nandP3TB();
  reg aa,bb;
  wire ww;
  nandP1 G3(aa,bb,ww);
  initial begin
    aa=0;
    bb=0;
    #150;
    aa=0;
    bb=1;
    #150;
    aa=1;
    bb=1;
    #150;
    aa=1;
    bb=0;
    #150; 
    $stop;
  end
endmodule

`timescale 1 ns / 1 ps
module MuxP4TB();
  reg ss,aa,bb;
  wire ww;
  MuxP4 m1(aa,bb,ss,ww);
  initial begin
    aa=0;
    bb=0;
    ss=0;
    #150;
    aa=0;
    bb=0;
    ss=1;
    #150;
    aa=0;
    bb=1;
    ss=1;
    #150;
    aa=0;
    bb=1;
    ss=0;
    #150;
    aa=1;
    bb=1;
    ss=0;
    #150;
    aa=1;
    bb=1;
    ss=1;
    #150;
    aa=1;
    bb=0;
    ss=1;
    #150;
    aa=1;
    bb=0;
    ss=0;
    #150;
    $stop;
  end
endmodule

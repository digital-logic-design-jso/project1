`timescale 1 ns / 1 ps
module ANOTB();
  reg aa,bb;
  wire ww,jj,kk;
  ANO ano(aa,bb,ww,jj,kk);
  initial begin 
    aa=0;
    bb=0;
    #150;
    aa=0;
    bb=1;
    #150;
    aa=1;
    bb=1;
    #150;
    aa=1;
    bb=0;
    #150;
    $stop;
  end
endmodule

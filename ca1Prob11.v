`timescale 1 ns / 1 ps
module ANO (input a,b, output w,j,k);
  MuxP9 mAnd(0,b,a,w);
  MuxP9 mOr(b,1,a,j);
  MuxP9 mXor(a,~a,b,k);
endmodule

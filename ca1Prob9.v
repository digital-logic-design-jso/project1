`timescale 1 ns / 1 ps
module MuxP9 (input a,b,s , output w);
  assign #(30,24) w=(s & b)|(~s & a);
endmodule
